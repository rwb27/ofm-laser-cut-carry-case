# Travel box for OpenFlexure Microscope
This repo contains designs for a laser-cut box with foam inserts, intended to hold an OpenFlexure Microscope (v6), mostly for transportation but with some cut-outs in the inner box that should allow it to be used without removing it from the box completely.

## Building one
The laser cuttable design is in ``TwoPiece.svg`` (and dxf), intended for 5mm acrylic (I think the inner box in particular works nicely if it's transparent).  With our 80W CO2 laser cutter, we use 7mm/s and 64% power to cut the acrylic.

The foam inserts are made by ``foam_cutouts.scad`` and should be saved in ``foam_cutouts.dxf``, which you can then glue together (three at the top, two at the bottom) to hold the microscope securely inside the box.  I used 12mm EVA foam floor tiles, which seem to work pretty nicely.  On our 80W CO2 laser, cutting them at 30% power, 50mm/s (or 60% power, 100mm/s) worked, though we had to run the cut 3 times to get all the way through the foam.  It seems that running multiple quick cuts works much better than one slow cut - perhaps because it allows the vapourised material to escape in between cuts.

> **IMPORTANT SAFETY WARNING**
> Please take care that the foam you use does not contain PVC or other chlorine-containing plastics.  Cutting PVC on a laser cutter releases chlorine gas, which is highly toxic and may kill you.  It is **NOT** removed by a HEPA filter, so if you are using an extractor with a filtration unit, that vents into your workshop, you must absolutely never cut anything containing chlorine.  Even if your laser cutter vents outdoors, this is probably a bad idea.  EVA foam shouldn't contain chlorine, but you should stay alert for the smell - and if you do smell it, stop the cut and leave the area until the gas (and smell) has dissipated.

## Development notes/instructions
This was quickly knocked up using the excellent [box generator](https://www.festi.info/boxes.py/TwoPiece?language=en) to save a box to SVG, then edited with Inkscape.  The settings I used are in the web page in ``box-generator``.  The festi.info website doesn't seem to be available any more, but ``boxes.py`` is still obtainable, if you run it yourself.

The first box was about right, the second one I cut seems to have worked better - maybe the laser was giving more output power somehow?  The first on didn't cut all the way through.  I've flipped the logos so they are the right way up now.

The OpenSCAD file for the foam inserts depends on the openflexure microscope - rather than mess around with git submodules, it assumes that you've cloned the OpenFlexure Microscope repository into the same parent folder as this repository - so it references those files as ``../openflexure-microscope/openscad/``.  If you want this to work properly for you, you should:

1. Clone this repository onto your computer
2. Clone https://gitlab.com/openflexure/openflexure-microscope/ onto your computer, in the same place
3. You should now have a folder on your computer that contains ``ofm-laser-cut-carry-case`` and ``openflexure-microscope`` as subfolders
4. Hopefully if you open ``ofm-laser-cut-carry-case/foam_cutouts.scad`` and render the design, it shouldn't give you any errors relating to included files.

