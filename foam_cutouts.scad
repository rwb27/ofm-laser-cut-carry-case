/*

This is a case for the OpenFlexure Microscope

This file defines various things relating to the foam inserts.

Released under CERN open hardware license, 2020
*/

use <../openflexure-microscope/openscad/main_body.scad>;
use <../openflexure-microscope/openscad/microscope_stand.scad>;
use <../openflexure-microscope/openscad/main_body_transforms.scad>;
use <../openflexure-microscope/openscad/gears.scad>;
use <../openflexure-microscope/openscad/z_axis.scad>;
include <../openflexure-microscope/openscad/microscope_parameters.scad>;
use <../openflexure-microscope/openscad/utilities.scad>;
use <../openflexure-microscope/openscad/illumination.scad>;

box_inner_size = [158, 146, 212];
box_inner_centre = [0, 18];

foam_thickness = 12; // This is used to make sure we allow for the right amount of the microscope - it isn't too critical.

module foam_outer(){
    // I was toying with a closer-fitting curvy design, but that might be harder!
    offset(15) projection(){
        main_body();
        microscope_stand();

        each_actuator() translate([0,actuating_nut_r,0]){
            motor_and_gear_clearance(h=20);
        }
        z_motor_clearance(motor_h=20);
    }
}

module feet_cutouts(){
    // Holes to avoid knocking the little rubber feet, if present
    projection() reflect([1,0,0]) leg_frame(45) translate([0, actuating_nut_r, 0]){
        cylinder(d=15, h=20);
    }
    translate([0, illumination_clip_y-14+7]) circle(d=15);
}

module box_inner(){
    translate(box_inner_centre) square([box_inner_size[0], box_inner_size[1]], center=true);
}

module slide_holder(){
    slide = [26, 76];
    translate(box_inner_centre - box_inner_size/2 + slide/2 + [10, 10]) union(){
        square(slide - [0, 15], center=true);
        repeat([slide[0]/6, 0], 6, center=true) square([0.2, slide[1] - 4], center=true);
    }
}

module box_insert_with_feet(){
    difference(){
        box_inner();
        feet_cutouts();
    }
}
module box_insert_with_bucket(){
    difference(){
        box_inner();
        
        footprint();

        // cutout for cables
        projection() pi_frame() hull(){
            translate([15, -30, 0]) cylinder(r=5, h=1, $fn=32);
            translate([15+25, -30, 0]) cylinder(r=5, h=1, $fn=32);
            translate([10, 0, 0]) cube([35,1,1]);
        }
    }
}

module box_insert_with_illumination(dovetail=true, slideholder=false){
    difference(){
        box_inner();

        offset(5) projection(){
            tall_condenser(bottom=false);
            if(dovetail) intersection(){
                illumination_arm();
                translate([0,0,illumination_arm_screws[0][2] + 45 - foam_thickness]) cylinder(h=999, r=999, $fn=5);
            }
        }

        if(slideholder) slide_holder();
    }
}

//foam_outer();
dx = box_inner_size[0] + 2;
translate([dx*0, 0]) box_insert_with_feet();
translate([dx*1, 0]) box_insert_with_bucket();
translate([dx*2, 0]) box_insert_with_illumination(dovetail=false, slideholder=false);
translate([dx*3, 0]) box_insert_with_illumination(dovetail=false, slideholder=true);
translate([dx*4, 0]) box_insert_with_illumination(dovetail=true, slideholder=true);